$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("SearchCars.feature");
formatter.feature({
  "line": 2,
  "name": "Accept test validation for Search cars page",
  "description": "In order to validate that\r\nthe search cars page is working\r\nWe will do the acceptance testing",
  "id": "accept-test-validation-for-search-cars-page",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Search-Cars"
    }
  ]
});
formatter.before({
  "duration": 13480091400,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Validate search cars page",
  "description": "",
  "id": "accept-test-validation-for-search-cars-page;validate-search-cars-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 7,
      "name": "@Search-Cars-Positive"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "I am on the Home page \"https://www.carsguide.com.au\" of CarsGuide website",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I move to the menu",
  "rows": [
    {
      "cells": [
        "Menu"
      ],
      "line": 11
    },
    {
      "cells": [
        "Cars for sale"
      ],
      "line": 12
    },
    {
      "cells": [
        "Sell My Car"
      ],
      "line": 13
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "click on \"Search Cars\" link",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "select cardbrand as \"BMW\" from AnyMake dropdown",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "select carmodel as \"1 Series\" from SelectModel dropdown",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "select location as \"NSW - Sydney\" from SelectLocation dropdown",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "select price as \"$1,000\" from price dropdown",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "click on Find_MY_Next_Car_Button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I should see list search result cars",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "the page title should be \"Bmw 1 Series Under 1000 for Sale Sydney NSW | carsguide\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.carsguide.com.au",
      "offset": 23
    }
  ],
  "location": "SearchCarsSteps.i_am_on_the_Home_page_of_CarsGuide_website(String)"
});
formatter.result({
  "duration": 10678891400,
  "status": "passed"
});
formatter.match({
  "location": "SearchCarsSteps.i_move_to_the_menu(String\u003e)"
});
formatter.result({
  "duration": 2158368200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Search Cars",
      "offset": 10
    }
  ],
  "location": "SearchCarsSteps.click_on_link(String)"
});
formatter.result({
  "duration": 5909660000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BMW",
      "offset": 21
    }
  ],
  "location": "SearchCarsSteps.select_cardbrand_as_from_AnyMake_dropdown(String)"
});
formatter.result({
  "duration": 562435300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1 Series",
      "offset": 20
    }
  ],
  "location": "SearchCarsSteps.select_carmodel_as_from_SelectModel_dropdown(String)"
});
formatter.result({
  "duration": 591660500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NSW - Sydney",
      "offset": 20
    }
  ],
  "location": "SearchCarsSteps.select_location_as_from_SelectLocation_dropdown(String)"
});
formatter.result({
  "duration": 480724200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$1,000",
      "offset": 17
    }
  ],
  "location": "SearchCarsSteps.select_price_as_from_price_dropdown(String)"
});
formatter.result({
  "duration": 454880600,
  "status": "passed"
});
formatter.match({
  "location": "SearchCarsSteps.click_on_Find_MY_Next_Car_Button()"
});
formatter.result({
  "duration": 2122653700,
  "status": "passed"
});
formatter.match({
  "location": "SearchCarsSteps.i_should_see_list_search_result_cars()"
});
formatter.result({
  "duration": 135000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bmw 1 Series Under 1000 for Sale Sydney NSW | carsguide",
      "offset": 26
    }
  ],
  "location": "SearchCarsSteps.the_page_title_should_be(String)"
});
formatter.result({
  "duration": 777448600,
  "status": "passed"
});
formatter.after({
  "duration": 5032895400,
  "status": "passed"
});
formatter.uri("UsedCarSaleSearch.feature");
formatter.feature({
  "line": 2,
  "name": "Acceptance testing to validate Search Cars Page is working.",
  "description": "In order to validate that \r\nthe Used Search Cars page is working\r\nPerforming the Acceptance Testing",
  "id": "acceptance-testing-to-validate-search-cars-page-is-working.",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Used-Car-Search"
    }
  ]
});
formatter.before({
  "duration": 9970545400,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Validate Used  Search Cars Page",
  "description": "",
  "id": "acceptance-testing-to-validate-search-cars-page-is-working.;validate-used--search-cars-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 7,
      "name": "@Used-Car-SearchPositive"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "I navigate to the  Home Page \"https://www.carsguide.com.au\" of CarsGuide Website",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I move to Car-For-Sale Menu",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "click on submenu Used Cars Search link",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "select carMaker from AnyMake dropdown",
  "rows": [
    {
      "cells": [
        "carMaker"
      ],
      "line": 13
    },
    {
      "cells": [
        "Audi"
      ],
      "line": 14
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "select  car model",
  "rows": [
    {
      "cells": [
        "carModel"
      ],
      "line": 16
    },
    {
      "cells": [
        "A4"
      ],
      "line": 17
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "select location  from SelectLocation dropdown",
  "rows": [
    {
      "cells": [
        "location"
      ],
      "line": 19
    },
    {
      "cells": [
        "SA - All"
      ],
      "line": 20
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "select price",
  "rows": [
    {
      "cells": [
        "price"
      ],
      "line": 22
    },
    {
      "cells": [
        "$1,000"
      ],
      "line": 23
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "click on Find_My_Car button",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I should see list of Used cars",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "the page title should match expected title  \"Used Audi A4 Under 1000 for Sale SA | carsguide\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.carsguide.com.au",
      "offset": 30
    }
  ],
  "location": "UsedCarSearchSteps.i_navigate_to_the_Home_Page_of_CarsGuide_Website(String)"
});
formatter.result({
  "duration": 7506408500,
  "status": "passed"
});
formatter.match({
  "location": "UsedCarSearchSteps.i_move_to_Car_For_Sale_Menu()"
});
formatter.result({
  "duration": 1014833000,
  "status": "passed"
});
formatter.match({
  "location": "UsedCarSearchSteps.click_on_submenu_Used_Cars_Search_link()"
});
formatter.result({
  "duration": 5408185400,
  "status": "passed"
});
formatter.match({
  "location": "UsedCarSearchSteps.select_carMaker_from_AnyMake_dropdown(String\u003e)"
});
formatter.result({
  "duration": 769754400,
  "status": "passed"
});
formatter.match({
  "location": "UsedCarSearchSteps.select_car_model(String\u003e)"
});
formatter.result({
  "duration": 357459300,
  "status": "passed"
});
formatter.match({
  "location": "UsedCarSearchSteps.select_location_from_SelectLocation_dropdown(String\u003e)"
});
formatter.result({
  "duration": 434321800,
  "status": "passed"
});
formatter.match({
  "location": "UsedCarSearchSteps.select_price(String\u003e)"
});
formatter.result({
  "duration": 1005343200,
  "status": "passed"
});
formatter.match({
  "location": "UsedCarSearchSteps.click_on_Find_My_Car_button()"
});
formatter.result({
  "duration": 7737990700,
  "status": "passed"
});
formatter.match({
  "location": "UsedCarSearchSteps.i_should_see_list_of_Used_cars()"
});
formatter.result({
  "duration": 149200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Used Audi A4 Under 1000 for Sale SA | carsguide",
      "offset": 45
    }
  ],
  "location": "UsedCarSearchSteps.the_page_title_should_match_expected_title(String)"
});
formatter.result({
  "duration": 164878000,
  "status": "passed"
});
formatter.after({
  "duration": 2584715000,
  "status": "passed"
});
});