@Search-Cars
Feature: Accept test validation for Search cars page
	In order to validate that
	the search cars page is working
	We will do the acceptance testing
	
@Search-Cars-Positive
Scenario: Validate search cars page
	Given I am on the Home page "https://www.carsguide.com.au" of CarsGuide website
	When I move to the menu
	| Menu|
	|Cars for sale|
	|Sell My Car|
	
	And click on "Search Cars" link
	And select cardbrand as "BMW" from AnyMake dropdown
	And select carmodel as "1 Series" from SelectModel dropdown
	And select location as "NSW - Sydney" from SelectLocation dropdown
	And select price as "$1,000" from price dropdown
	And click on Find_MY_Next_Car_Button
	Then I should see list search result cars
	And the page title should be "Bmw 1 Series Under 1000 for Sale Sydney NSW | carsguide"