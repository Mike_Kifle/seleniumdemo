package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CarsSearchPageLocators {
	

	@FindBy(how=How.XPATH,using="//*[@id='makes']")
	public WebElement carMakeDropdown;
	
	@FindBy(how=How.XPATH,using="//select[@id='models']")
	public WebElement carModelsDropdown;
	
	@FindBy(how=How.XPATH,using="//*[@id='locations']")
	public WebElement selectLocationDropdown;
	
	@FindBy(how=How.XPATH,using="//*[@id='priceTo']")
	public WebElement priceDropdown;
	
	@FindBy(how=How.XPATH,using="//*[@id='search-submit']")
	public WebElement findMyNextCarBtn;
	
	
}
