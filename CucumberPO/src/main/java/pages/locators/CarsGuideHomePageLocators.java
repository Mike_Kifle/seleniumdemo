package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CarsGuideHomePageLocators {

	@FindBy(how=How.XPATH,using="//a[contains(text(),'buy + sell')]")
	public WebElement buyorsellcars;
	
	@FindBy(how=How.XPATH,using="//a[contains(text(),'Cars For Sale']")
	public WebElement carsForSaleLink;
	
	@FindBy(how=How.XPATH,using="//a[contains(text(),'Sell My Car')]")
	public WebElement sellMyCarLink;
	
	@FindBy(how=How.XPATH,using="//a[contains(text(),'Car Reviews')]")
	public WebElement carReviewLink;
	
	@FindBy(how=How.XPATH,using="//a[contains(text(),'Search Cars')]")
	public WebElement searchCarsLink;
	
	@FindBy(how=How.XPATH,using="//a[contains(text(),'Used')]")
	public WebElement usedCarsLink;
	
	@FindBy(how=How.XPATH,using="//div[contains(text(),'BMW')]\"));")
	public WebElement CarTypeFound;
}
