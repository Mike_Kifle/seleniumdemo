package pages.actions;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import pages.locators.CarsSearchPageLocators;
import utils.SeleniumDriver;

public class CarsSearchPageActions {

	CarsSearchPageLocators carsSearchPageLocators = null;

	public CarsSearchPageActions() {
		this.carsSearchPageLocators = new CarsSearchPageLocators();
		PageFactory.initElements(SeleniumDriver.getDriver(), carsSearchPageLocators);
	}

	public void selectCarMake(String carBrand) {

		Select selectCarMaker = new Select(carsSearchPageLocators.carMakeDropdown);
		selectCarMaker.selectByVisibleText(carBrand);

	}

	public void selectCarModel(String carModel) {

		Select selectCarMaker = new Select(carsSearchPageLocators.carModelsDropdown);
		carsSearchPageLocators.carModelsDropdown.click();
		selectCarMaker.selectByVisibleText(carModel);

	}

	public void selectcarLocation(String location) {

		Select selectCarMaker = new Select(carsSearchPageLocators.selectLocationDropdown);
		carsSearchPageLocators.selectLocationDropdown.click();
		selectCarMaker.selectByVisibleText(location);

	}

	public void selectCarPrice(String carPrice) {

		Select selectCarMaker = new Select(carsSearchPageLocators.priceDropdown);
		carsSearchPageLocators.priceDropdown.click();
		selectCarMaker.selectByVisibleText(carPrice);

	}
	
	public void clickOnFindMyNextCarButton() {
		
		carsSearchPageLocators.findMyNextCarBtn.click();
	}
}
